import { browser, by, element, protractor } from 'protractor';

describe('weatherapp App', () => {

  beforeEach(function () {
    browser.get('/');
  });

  function mockGeo(lat, lon) {
    return 'window.navigator.geolocation.getCurrentPosition = ' +
      '       function (success, error) {' +
      '           var position = {' +
      '               "coords" : {' +
      '                   "latitude": "' + lat + '",' +
      '                   "longitude": "' + lon + '"' +
      '               }' +
      '           };' +
      '           success(position);' +
      '       }';
  }

  it('should display welcome message', () => {
    expect(element(by.css('app-root h1')).getText()).toEqual('My Weather App!');
  });

  it('should show local weather', () => {
    browser.executeScript(mockGeo(52.36, 4.87));
    setTimeout(function() {
      const firstcard = element.all(by.tagName('md-card')).first();
      expect(firstcard.getText()).toContain('Amsterdam');  
    }, 100);

  });

  xit('should navigate to new location page', () => {
    element(by.css('[routerlink="/newlocation"]')).click().then(function () {
      expect(element(by.tagName('agm-map')).isPresent()).toBeTruthy();
    });
  });

  xit('should navigate to new location page and add new city', () => {
    element(by.css('[routerlink="/newlocation"]')).click().then(function () {
      const searchfield = element(by.css('[formcontrolname="cityname"]'));
      searchfield.sendKeys('delhi');
      searchfield.sendKeys(protractor.Key.ARROW_DOWN);
      searchfield.sendKeys(protractor.Key.TAB);
      element(by.tagName('button')).click().then(function () {
        const lastcard = element.all(by.tagName('md-card')).last();
        expect(lastcard.getText()).toContain('Delhi');
      });
    });

  });


});

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  appid: 'dec6a549903f3824c3ce7745acd22684',
  appurl: 'http://api.openweathermap.org/data/2.5/weather',
  googleplacesappid: 'AIzaSyAWX-N-1crwxPeTmvdlljugxSV7eDZty7Y',
  goolgeplacesurl: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
  googleplaceurl: 'https://maps.googleapis.com/maps/api/place/details/json'
};

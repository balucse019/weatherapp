import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyWeatherListComponent } from './my-weather-list.component';

describe('MyWeatherListComponent', () => {
  let component: MyWeatherListComponent;
  let fixture: ComponentFixture<MyWeatherListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyWeatherListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyWeatherListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Weather } from '../weather';
import { MdSnackBar } from '@angular/material';
import { City } from '../city';
import { LocalCityStorageService } from '../local-storage.service';

@Component({
  selector: 'app-my-weather-list',
  templateUrl: './my-weather-list.component.html',
  styleUrls: ['./my-weather-list.component.scss']
})
export class MyWeatherListComponent implements OnInit {

  cityList: City[] = [];

  constructor(private storeService: LocalCityStorageService) {
    this.setCurrentCity();
    this.cityList = this.storeService.cities || [];
  }

  setCurrentCity() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pos => {
        const city = new City();
        city.coord = {
          lat: pos.coords.latitude,
          lon: pos.coords.longitude
        };
        this.storeService.addCity(city);
      });
    }
  }

  ngOnInit() {

  }


}

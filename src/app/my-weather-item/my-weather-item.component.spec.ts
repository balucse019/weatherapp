import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyWeatherItemComponent } from './my-weather-item.component';

describe('MyWeatherItemComponent', () => {
  let component: MyWeatherItemComponent;
  let fixture: ComponentFixture<MyWeatherItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyWeatherItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyWeatherItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

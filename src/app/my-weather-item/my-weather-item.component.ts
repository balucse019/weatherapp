import { Component, OnInit, Input } from '@angular/core';
import { MdCardModule } from '@angular/material';
import { City } from '../city';
import { Weather } from '../weather';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-my-weather-item',
  templateUrl: './my-weather-item.component.html',
  styleUrls: ['./my-weather-item.component.scss']
})
export class MyWeatherItemComponent implements OnInit {

  @Input() city: City;
  weatherInfo: Weather;

  constructor(private ws: WeatherService) {
  }

  ngOnInit() {
    console.log('weather item');
    console.log(this.city);
    if (this.city !== undefined) {
      this.getWeatherByCoords();
    }
  }

  getWeatherByCoords() {
    this.ws.searchByGeoCoord(this.city.coord.lat, this.city.coord.lon).
      subscribe(res => this.weatherInfo = res);
  }

}

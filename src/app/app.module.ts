import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyWeatherListComponent } from './my-weather-list/my-weather-list.component';
import { MyWeatherItemComponent } from './my-weather-item/my-weather-item.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import {WeatherService} from './weather.service';
import {HttpModule} from '@angular/http';
import { CitySearchComponent } from './city-search/city-search.component';
import {MaterialModule, MdButtonModule, MdAutocompleteModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocalCityStorageService} from './local-storage.service';
import {GooglePlaceService} from './google-place.service';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MyWeatherListComponent,
    MyWeatherItemComponent,
    PageNotFoundComponent,
    CitySearchComponent
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleplacesappid,
      libraries: ['places']
    }),
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MdButtonModule,
    MdAutocompleteModule,
    BrowserAnimationsModule
  ],
  providers: [WeatherService, LocalCityStorageService, GooglePlaceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

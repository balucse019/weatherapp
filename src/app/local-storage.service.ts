import { Injectable } from '@angular/core';
import { City } from './city';

@Injectable()
export class LocalCityStorageService {

  public cities: City[] = [];

  cityStorageKey = 'wp.cities';

  constructor() {
    console.log('LocalCityStorageService');
    this.cities = this.getAllCitiesFromStorage() || [];
  }

  addCity(city: City): void {

    city.coord.lat = Number(city.coord.lat.toFixed(2));
    city.coord.lon = Number(city.coord.lon.toFixed(2));

    if (this.exists(city)) {
      return;
    }

    this.cities.push(city);

    if (typeof Storage) {
      localStorage.setItem(this.cityStorageKey, JSON.stringify(this.cities));
    }

  }

  exists(city: City): boolean {

    const exists = this.cities.find(function (ele) {
      return (ele.coord.lat === city.coord.lat) && (ele.coord.lon === city.coord.lon);
    });

    return (exists !== undefined) ? true : false;

  }

  getAllCitiesFromStorage(): City[] {
    if (typeof Storage) {
      try {
        return JSON.parse(localStorage.getItem(this.cityStorageKey));
      } catch (e) {
        return [];
      }
    } else {
      return [];
    }
  }

}

import { Component, OnInit, Output, EventEmitter, NgZone, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { GooglePlaceService } from '../google-place.service';
import {LocalCityStorageService} from '../local-storage.service';
import { City } from '../city';
import { MdOptionSelectionChange } from '@angular/material';
import { MapsAPILoader } from '@agm/core';
import { } from '@types/googlemaps';
import {Router} from '@angular/router';


@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.scss']
})
export class CitySearchComponent implements OnInit {

  filteredPlaces: Observable<City[]>;
  selectedCity: City;
  cityForm: FormGroup;

  public latitude: number;
  public longitude: number;
  public zoom: number;

  // @Output()
  // newCityAdded: EventEmitter<City> = new EventEmitter<City>();

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private googps: GooglePlaceService, private fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private cityStorageService: LocalCityStorageService,
    private router: Router) {
  }


  setCurrentCity() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(pos => {

        this.latitude = pos.coords.latitude;
        this.longitude = pos.coords.longitude;
      });
    }
  }

  addCity() {
    const city = new City();
    city.coord = {
      lat: this.latitude,
      lon: this.longitude
    };
    this.cityStorageService.addCity(city);
    this.router.navigate(['/mycities']);
  }

  ngOnInit() {
    this.zoom = 4;
    this.createForm();
    this.setCurrentCity();

    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['geocode']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }


  createForm() {
    this.cityForm = this.fb.group({
      cityname: ['', Validators.required]
    });
  }

}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { environment } from '../environments/environment';
import { Weather } from './weather';
import * as mock from './mock.data';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';

@Injectable()
export class WeatherService {

  constructor(private http: Http) {
  }


  searchByCity(city: string): Observable<Weather> {
    return this.http.get(environment.appurl + '?q=' + city + '&appid=' + environment.appid)
      .map(res => res.json());
  }

  searchByGeoCoord(lat: number, lon: number): Observable<Weather> {
      return this.http.get(environment.appurl + '?lat=' + lat + '&lon=' + lon +
        '&appid=' + environment.appid + '&units=metric').map(res => res.json());
          // return of(mock.mockweather);
  }

}

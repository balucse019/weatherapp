import { TestBed, inject } from '@angular/core/testing';

import { LocalCityStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalCityStorageService]
    });
  });

  it('should be created', inject([LocalCityStorageService], (service: LocalCityStorageService) => {
    expect(service).toBeTruthy();
  }));
});

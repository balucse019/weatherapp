export class Weather {

    coord: { lon: number, lat: number };
    base?: string;
    sys?: any;
    weather: { id: number, main: string, description: string, icon: string }[];
    name: string;
    id: number;
    dt: number;
    main: {
        temp: number, humidity: number, pressure: number, temp_min: number,
        temp_max: number, sea_level?: number, grnd_level?: number
    };
    wind?: { speed: number, deg: number };
    cod: number;
    visibility: number;
    clouds: any;


}

import { Weather } from './weather';


export const mockweather: Weather = {
    'coord': { 'lon': 4.87, 'lat': 52.36 },
    'weather': [{ 'id': 801, 'main': 'Clouds', 'description': 'few clouds', 'icon': '02d' }],
    'base': 'stations',
    'main': {
        'temp': 296.14, 'pressure': 1015, 'humidity': 60,
        'temp_min': 295.15, 'temp_max': 297.15
    },
    'visibility': 10000, 'wind': { 'speed': 5.1, 'deg': 280 },
    'clouds': { 'all': 20 }, 'dt': 1503757500,
    'sys': {
        'type': 1, 'id': 5204,
        'message': 0.0025, 'country': 'NL',
        'sunrise': 1503722573, 'sunset': 1503772811
    },
    'id': 2759793, 'name': 'Gemeente Amsterdam',
    'cod': 200
};


export const mockplaces: any = {
    predictions:
    [{ description: 'Paris, France', place_id: 'ChIJD7fiBh9u5kcRYJSMaMOCCwQ' },
    { description: 'Paris Avenue, Earlwood, New South Wales, Australia', place_id: 'ChIJrU3KAHG6EmsR5Uwfrk7azrI' },
    { description: 'Paris Street, Carlton, New South Wales, Australia', place_id: 'ChIJCfeffMi5EmsRp7ykjcnb3VY' }
    ]
};

export const mockplace: any = {
    results: [{ geometry: { location: { lat: -33.870775, lng: 151.199025 } } }]
};



import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MyWeatherListComponent } from './my-weather-list/my-weather-list.component';
import { CitySearchComponent } from './city-search/city-search.component';

const routes: Routes = [
  { path: 'mycities', component: MyWeatherListComponent},
  { path: 'newlocation', component: CitySearchComponent},
  { path: '', redirectTo: '/mycities', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

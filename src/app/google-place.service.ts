import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';
import 'rxjs/Rx';
import { environment } from '../environments/environment';
import { City } from './city';
import { Http } from '@angular/http';
import * as mock from './mock.data';

@Injectable()
export class GooglePlaceService {

  constructor(private http: Http) {
  }

  findPlaces(input: string): Observable<City[]> {
    // return this.http.get(environment.goolgeplacesurl + '?input=' + input
    //   + '&types=(cities)&key=' + environment.googleplacesappid)
    //   .map(res => res.json()).flatMap(res => Observable.from(res.predictions))
    //   .map((res: any) => {
    //     const city = new City();
    //     city.name = res.description;
    //     city.googleplaceid = res.place_id;
    //     return city;
    //   }).toArray();

    return of(mock.mockplaces).flatMap(res => Observable.from(res.predictions))
      .map((res: any) => {
        const city = new City();
        city.name = res.description;
        city.googleplaceid = res.place_id;
        return city;
      }).toArray();

  }

  findCoordinates(placeid: string): Observable<any> {
    return this.http.get(environment.googleplaceurl + '?placeid=' + placeid
      + '&key=' + environment.googleplacesappid)
      .map(res => res.json()).flatMap(res => Observable.of(res.results[0])).
      map(res => {
        return { lat: res.geometry.location.lat, lon: res.geometry.location.lng };
      });
    // return of(mock.mockplace).flatMap(res => Observable.of(res.results[0])).
    // map(res => {
    //   return { lat: res.geometry.location.lat, lon: res.geometry.location.lng};
    // });
  }

}
